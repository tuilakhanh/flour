module("luci.controller.flour", package.seeall)

function index()
		page = node("admin", "network", "Flour")
		page.target = cbi("admin_network/flour")
		page.title = _("Flour")
		page.order = 70

		page = entry({"admin", "network", "flour_iface_dump_start"}, call("flour_iface_dump_start"), nil)
		page.leaf = true

		page = entry({"admin", "network", "flour_iface_dump_stop"}, call("flour_iface_dump_stop"), nil)
		page.leaf = true

		page = entry({"admin", "network", "flour_check_status"}, call("flour_check_status"), nil)
		page.leaf = true

		page = entry({"admin", "network", "flour_link_list_get"}, call("flour_link_list_get"), nil)
		page.leaf = true

		page = entry({"admin", "network", "flour_link_list_clear"}, call("flour_link_list_clear"), nil)
		page.leaf = true
end

function flour_iface_dump_start(ifname, value, flag, filter)
	if ifname == nil or ifname == '' then
		ifname = 'any'
	end
	if tonumber(value) == nil
	then
		value = '0'
	end
	if filter == nil or filter == '' then
		filter = ''
	end

	if flag == nil or flag == '' then
		filter = 'T'
	end

	luci.http.prepare_content("text/plain")

	local res = os.execute("(/sbin/flour -i " .. ifname .. " -" .. flag .. " " .. value .. " -p /tmp/flour-luci.pid " .. filter .. " > /tmp/flour-luci.out 2>&1) &")
	luci.http.write(tostring(res))
end

function flour_iface_dump_stop()
	luci.http.prepare_content("text/plain")

	local f = io.open("/tmp/flour-luci.pid", "rb")
	local pid = f:read("*all")
	io.close(f)

	local res = os.execute("kill -TERM " .. pid)
	luci.http.write(tostring(res))
end

function flour_check_status()

	local msg = "";
	local status;
	local f = io.open("/tmp/flour-luci.pid","r")
	if f ~= nil then
		status = 1;
		io.close(f)
	else
		status = 0;
	end

	f = io.open("/tmp/flour-luci.out","r")
	if f ~= nil then
		msg = f:read("*all")
		io.close(f)
		if msg ~= '' then
			os.remove('/tmp/flour-luci.out')
		end
	end

	luci.http.prepare_content("application/json")

	local res = {}
	res["status"] = status;
	res["msg"] = msg;

	luci.http.write_json(res)
end
