local fs = require "nixio.fs"

m = Map("flour", translate("Flour"))

if fs.access("/etc/config/flour") then
	m:section(SimpleSection).template = "flour"

	s = m:section(TypedSection, "flour", translate("Options"))
	s.anonymous = true
	s.addremove = false

	s:option(Value, "url", translate("URL"))
	s:option(Value, "token", translate("API token"))
end

return m
