#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <pcap.h>

#include <libubox/uloop.h>
#include <libubox/uclient.h>

#include "config.h"
#include "flour.h"
#include "pcap.h"
#include "uclient.h"

struct flour flour;

static void show_help()
{
	printf("usage: %s [-iwskTSph] [ expression ]\n\n%s", PROJECT_NAME, \
		"  -i  listen on interface\n" \
		"  -w  write the raw packets to specific file\n" \
		"  -s  snarf snaplen bytes of data\n" \
		"  -k  keep the file after uploading it to trusted client\n" \
		"  -T  stop capture after this many seconds have passed, use 0 for no timeout (Defaut: 0)\n" \
		"  -S  stop capture after this many bytes have been saved, use 0 for no limit (Defaut: 0)\n" \
		"  -p  save pid to a file\n" \
		"  -h  shows this help\n");
}

static void dump_timeout_callback(struct uloop_timeout *t)
{
	DEBUG("timeout reached, stopping capture\n");
	uloop_end();
}

int main(int argc, char *argv[])
{
	int rc, c;
	int keep = 0;
	char *pid_filename = NULL;

	/* zero out main struct */
	memset(&flour, 0, sizeof(flour));

	/* preconfigure defaults */
	flour.interface = "any";
	flour.filename = NULL;
	flour.snaplen = 65535;
	flour.filter = NULL;
	flour.packets = 0;
	flour.caplen = 0;
	flour.limit_caplen = 0;

	openlog(PROJECT_NAME, LOG_PERROR | LOG_PID, LOG_DAEMON);

	while ((c = getopt(argc, argv, "i:w:s:T:P:S:p:kvh")) != -1) {
		switch (c) {
			case 'i':
				flour.interface = optarg;
				break;

			case 'w':
				flour.filename = strdup(optarg);
				if (!flour.filename) {
					ERROR("not enough memory\n");
					rc = EXIT_FAILURE;
					goto exit;
				}
				break;

			case 's':
				flour.snaplen = atoi(optarg);
				if (!flour.snaplen) flour.snaplen = 65535;
				break;

			case 'T':
			{
				struct uloop_timeout dump_timeout = {
					.cb = dump_timeout_callback
				};

				int dump_timeout_s = atoi(optarg);
				if (dump_timeout_s > 0)
					uloop_timeout_set(&dump_timeout, dump_timeout_s * 1000);

				break;
			}

			case 'S':
				flour.limit_caplen = atoi(optarg);
				break;

			case 'p':
			{
				pid_t pid = getpid();

				pid_filename = optarg;
				FILE *f = fopen(pid_filename, "w");
				if (!f) {
					fprintf(stderr, "Failed writing PID to '%s'\n", optarg);
					return EXIT_FAILURE;
				}

				fprintf(f, "%d\n", pid);

				fclose(f);
				sync();

				break;
			}

			case 'k':
				keep = 1;
				break;

			case 'h':
				show_help();
				return EXIT_FAILURE;

			default:
				break;
		}
	}

	while (optind < argc) {
		asprintf(&flour.filter, "%s %s", \
			flour.filter ? flour.filter : "", argv[optind]);
		optind++;
	}

	rc = config_load();
	if (rc) {
		ERROR("unable to load configuration\n");
		rc = EXIT_FAILURE;
		goto exit;
	}

	if (!flour.filename) {
		int len = 0;
		len = snprintf(flour.filename, 0, "%s/flour.pcap-XXXXXX", config.dir);

		flour.filename = calloc(len + 1, sizeof(char));
		if (!flour.filename) {
			ERROR("not enough memory\n");
			rc = EXIT_FAILURE;
			goto exit;
		}
		snprintf(flour.filename, len + 1, "%s/flour.pcap-XXXXXX", config.dir);

		int fd = mkstemp(flour.filename);
		if (fd == -1) {
			ERROR("unable to create dump file\n");
			rc = EXIT_FAILURE;
			goto exit;
		}
	}

	uloop_init();

	rc = flour_pcap_init(&flour);
	if (rc) {
		rc = EXIT_FAILURE;
		goto exit;
	}

	printf("capturing traffic to file: '%s' ...\n", flour.filename);
	uloop_run();

	flour_pcap_done(&flour);
	printf("\n%lu packets captured\n", (long unsigned int) flour.packets);

	// rc = flour_uclient_init(&flour);
	// if (rc) {
	// 	rc = EXIT_FAILURE;
	// 	goto exit;
	// }
	printf("uploading capture ...\n");
	rc = flour_curl_upload(&flour);
	if (rc) {
		rc = EXIT_FAILURE;
		goto exit;
	}

	uloop_done();

	rc = EXIT_SUCCESS;

exit:
	flour_pcap_done(&flour);
	// flour_uclient_done(&flour);
	if (!keep) remove(flour.filename);
	free(flour.filename);
	if (pid_filename) remove(pid_filename);

	return rc;
}
