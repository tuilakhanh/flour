#ifndef __FLOUR_H__
#define __FLOUR_H__

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <syslog.h>

#include <pcap.h>

#include <libubox/uclient.h>

#define PROJECT_NAME "flour"
#define PROJECT_VERSION "v0.1"

struct flour {
	char *interface;
	char *filename;
	int snaplen;
	char *filter;

	pcap_t *p;
	pcap_dumper_t *p_dumper;
	struct bpf_program p_bfp;

	uint64_t packets;

	uint64_t caplen;
	uint64_t limit_caplen;

	struct uclient *ucl;
};

extern struct flour flour;

#ifdef WITH_DEBUG
#define DEBUG(fmt, ...) do { \
		fprintf(stderr, "%s: %s(%d): " fmt, PROJECT_NAME, __func__, __LINE__, ## __VA_ARGS__); \
	} while (0)
#else
#define DEBUG( ... )
#endif

#define LOG(fmt, ...) do { \
		syslog(0, fmt, ## __VA_ARGS__); \
		fprintf(stderr, fmt, ## __VA_ARGS__); \
	} while (0)

#define ERROR(fmt, ...) do { \
		syslog(0, fmt, ## __VA_ARGS__); \
		fprintf(stderr, fmt, ## __VA_ARGS__); \
	} while (0)


#ifndef __unused
#define __unused __attribute__((unused))
#endif

#define LIB_EXT "so"

#endif /* __FLOUR_H__ */
