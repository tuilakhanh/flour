#include "flour.h"
#include "uclient.h"
#include "config.h"

int flour_curl_upload(struct flour *cs) {
	char url[500];
	snprintf(url, 500, "%s/api/v1/%s/upload", config.url, config.token);
  	char curl_cmd[BUFSIZ];
	snprintf(curl_cmd, BUFSIZ, "curl -X POST -F 'file=@%s' %s", cs->filename, url);

	if (system(curl_cmd) != 0) {
    	ERROR("%s: curl command failed\n, %s", PROJECT_NAME, curl_cmd);
    	return -1;
  	}

	printf("... uploading completed!\n");
  	return 0;
}