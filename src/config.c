#include <stdlib.h>
#include <uci.h>
#include <string.h>
#include <libubox/blobmsg.h>
#include <uci_blob.h>

#include <libgen.h>
#include <sys/stat.h>

#include "config.h"

struct config config;

enum flour_config {
	FLOUR_URL,
	FLOUR_TOKEN,
	FLOUR_CA,
	FLOUR_CA_VERIFY,
	FLOUR_DIR,
	__FLOUR_MAX
};

const struct blobmsg_policy flour_policy[__FLOUR_MAX] = {
	[FLOUR_URL] = { .name = "url", .type = BLOBMSG_TYPE_STRING },
	[FLOUR_TOKEN] = { .name = "token", .type = BLOBMSG_TYPE_STRING },
	[FLOUR_CA] = { .name = "ca", .type = BLOBMSG_TYPE_STRING },
	[FLOUR_CA_VERIFY] = { .name = "ca_verify", .type = BLOBMSG_TYPE_BOOL },
	[FLOUR_DIR] = { .name = "dir", .type = BLOBMSG_TYPE_STRING },

};

const struct uci_blob_param_list config_attr_list = {
	.n_params = __FLOUR_MAX,
	.params = flour_policy
};

int config_load(void)
{
	int rc;
	struct uci_context *uci = uci_alloc_context();
	struct uci_package *conf = NULL;
	struct blob_attr *tb[__FLOUR_MAX], *c;
	static struct blob_buf buf;

	rc = uci_load(uci, "flour", &conf);
	if (rc) goto exit;

	blob_buf_init(&buf, 0);

	struct uci_element *section_elem;
	uci_foreach_element(&conf->sections, section_elem) {
		struct uci_section *s = uci_to_section(section_elem);
		uci_to_blob(&buf, s, &config_attr_list);
	}

	blobmsg_parse(flour_policy, __FLOUR_MAX, tb, blob_data(buf.head), blob_len(buf.head));

	if (!(c = tb[FLOUR_URL])) {
		rc = -1;
		goto exit;
	}
	snprintf(config.url, URL_MAX, "%s", blobmsg_get_string(c));

	/* we are adding '/' later in the code */
	if (config.url[strlen(config.url) - 1] == '/') {
		config.url[strlen(config.url) - 1] = 0;
	}

	if (!(c = tb[FLOUR_TOKEN])) {
		rc = -1;
		goto exit;
	}
	snprintf(config.token, TOKEN_MAX, "%s", blobmsg_get_string(c));

	/* ca option is optional */
	if (!(c = tb[FLOUR_CA])) {
		memset(config.ca, 0, PATH_MAX);
	} else {
		snprintf(config.ca, PATH_MAX, "%s", blobmsg_get_string(c));
	}

	/* ca_verify option is optional */
	if (!(c = tb[FLOUR_CA_VERIFY])) {
		config.ca_verify = true;
	} else {
		config.ca_verify = blobmsg_get_bool(c);
	}

	/* dir option is optional */
	if (!(c = tb[FLOUR_DIR])) {
		snprintf(config.dir, PATH_MAX, "/tmp");
	} else {
		snprintf(config.dir, PATH_MAX, "%s", blobmsg_get_string(c));
	}

	/* we are adding '/' later in the code */
	if (config.dir[strlen(config.dir) - 1] == '/') {
		config.dir[strlen(config.dir) - 1] = 0;
	}

	rc = 0;
exit:
	blob_buf_free(&buf);
	if (conf) (void) uci_unload(uci, conf);
	uci_free_context(uci);

	return rc;
}
