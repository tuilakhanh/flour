#ifndef __FLOUR_PCAP_H__
#define __FLOUR_PCAP_H__

#include <pcap.h>

#include "flour.h"

void flour_pcap_manage_packet(u_char *user, const struct pcap_pkthdr *header, const u_char *sp);
void flour_pcap_handle_packet_cb(struct uloop_fd *ufd, __unused unsigned int events);

int flour_pcap_init(struct flour *cs);
void flour_pcap_done(struct flour *cs);

extern struct uloop_fd ufd_pcap;

#endif /* __FLOUR_PCAP_H__ */
