#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <limits.h>

#define TOKEN_MAX 40
#define URL_MAX 8 + HOST_NAME_MAX + 7 + 1

int config_load(void);

struct config {
	char url[URL_MAX];
	char token[TOKEN_MAX];
	char ca[PATH_MAX];
	bool ca_verify;
	char dir[PATH_MAX];
};

extern struct config config;

#endif /* __CONFIG_H__ */
