#ifndef __FLOUR_UCLIENT_H__
#define __FLOUR_UCLIENT_H__

#include "flour.h"

int flour_curl_upload(struct flour *cs);

#endif /* __FLOUR_UCLIENT_H__ */
