#include <sys/vfs.h>

#include <libubox/uloop.h>

#include "flour.h"
#include "pcap.h"

struct uloop_fd ufd_pcap = { .cb = flour_pcap_handle_packet_cb };
static char *filename = NULL;

struct pcap_timeval {
	bpf_int32 tv_sec; /* seconds */
	bpf_int32 tv_usec; /* microseconds */
};

struct pcap_sf_pkthdr {
	struct pcap_timeval ts; /* time stamp */
	bpf_u_int32 caplen; /* length of portion present */
	bpf_u_int32 len; /* length this packet (off wire) */
};

void flour_pcap_manage_packet(u_char *user, const struct pcap_pkthdr *header, const u_char *sp)
{
	struct flour *cs = (struct flour *) user;
	static int stop_writing = false;
	static unsigned long captured_size = 0;
	struct statfs result;

	if (stop_writing) return;

	/* no need to check on every packet so check on every 10th that comes along */
	if (cs->packets % 10 == 0) {
		if (statfs(filename, &result) < 0 ) {
			ERROR("unable to determine free disk space for '%s'\n", filename);
			stop_writing = true;
			uloop_end();
			return;
		}

		/* leave a bit less then 512K of disk space available */
		if ((result.f_bsize * result.f_bfree) < captured_size) {
			DEBUG("stopping capture due to low disk space\n");
			stop_writing = true;
			uloop_end();
			return;
		}
	}

	captured_size += header->len;

	cs->packets++;

	cs->caplen += header->caplen;
	if (cs->limit_caplen && (cs->limit_caplen < cs->caplen)) {
		uloop_end();
		return;
	}

	/* pcap_dump does not handle errors so make fixes here instead */

	struct pcap_sf_pkthdr sf_hdr;
	size_t num = 0;

	sf_hdr.ts.tv_sec = header->ts.tv_sec;
	sf_hdr.ts.tv_usec = header->ts.tv_usec;
	sf_hdr.caplen = header->caplen;
	sf_hdr.len = header->len;

	num = fwrite(&sf_hdr, sizeof(sf_hdr), 1, (FILE *) cs->p_dumper);
	if (num != 1) {
		uloop_end();
		return;
	}

	num = fwrite(sp, header->caplen, 1, (FILE *) cs->p_dumper);
	if (num != 1) {
		uloop_end();
		return;
	}
}

void flour_pcap_handle_packet_cb(struct uloop_fd *ufd, __unused unsigned int events)
{
	int rc;

	rc = pcap_dispatch(flour.p, -1, flour_pcap_manage_packet, (u_char *) &flour);
	if (rc < 0) {
		uloop_end();
		return;
	}

	DEBUG("received '%d' packets\n", (int) flour.packets);
	DEBUG("received '%d' bytes\n", (int) flour.caplen);
}

int flour_pcap_init(struct flour *cs)
{
	int rc = -1;

	/* potential libpcap errors will end up here*/
	char e[PCAP_ERRBUF_SIZE];
	memset(e, 0, PCAP_ERRBUF_SIZE);

	/* open device in promiscuous mode */
	cs->p = pcap_open_live(cs->interface, cs->snaplen, 1, 0x0400, e);
	if (cs->p == NULL) {
		ERROR("pcap_open_live(): %s\n", e);
		goto exit;
	}

	if (cs->filter) {
		rc = pcap_compile(cs->p, &cs->p_bfp, cs->filter, 1, PCAP_NETMASK_UNKNOWN);
		if (rc == -1) {
			ERROR("pcap_compile(): could not parse filter\n");
			goto exit;
		}

		rc = pcap_setfilter(cs->p, &cs->p_bfp);
		if (rc == -1) {
			ERROR("pcap_setfilter(): could not parse filter\n");
			goto exit;
		}
	}

	cs->p_dumper = pcap_dump_open(cs->p, cs->filename);
	if (cs->p_dumper == NULL) {
		ERROR("pcap: could not open file for storing capture\n");
		rc = EXIT_FAILURE;
		goto exit;
	}

	/* we need to access this value in one of the callbacks */
	filename = cs->filename;

	/* set non-blocking state */
	rc = pcap_setnonblock(cs->p, 1, e);
	if (rc < 0) {
		ERROR("pcap_setnonblock(): %s\n", e);
		goto exit;
	}

	int socket;
	socket = pcap_get_selectable_fd(cs->p);
	if (socket < 0) {
		ERROR("pcap_get_selectable_fd(): invalid socket received\n");
		rc = -1;
		goto exit;
	}

	ufd_pcap.fd = socket;
	uloop_fd_add(&ufd_pcap, ULOOP_READ);

	rc = 0;
exit:
	return rc;
}

void flour_pcap_done(struct flour *cs)
{
	if (cs->p_dumper) {
		pcap_dump_close(cs->p_dumper);
		cs->p_dumper = NULL;
	}

	if (cs->p) {
		pcap_close(cs->p);
		cs->p = NULL;
	}
}
